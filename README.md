### Inspiration Links: ### 

## Websites: ##
- http://www.mooncampapp.com/
- http://creativeda.sh/
- http://wagerfield.github.io/parallax/
- http://www.mindworks.gr/
- http://redrain.tw/ 
- http://www.artconcept.com.pl/en/
- http://www.doubledouble.be/ /* Scroll, menu and the animated background */
- http://www.waaac.co/
- http://getgoldee.com/
- http://www.givakt.se/ /* Menu and thumbnail */
- http://www.wacom.com/ /* Social-Networks buttons */
- http://www.antro.ca/en/* Scroll, menu and the animated background  */
- http://www.lorenzobocchi.com/ /* Loading */
- http://www.myprovence.fr/snapshots2013/en/grid /* Menu */
- http://www.lorenzobocchi.com/
- http://www.10jourspoursigner.org
- http://fontwalk.de/
- http://soulwire.co.uk/experiments
- http://www.kennedyandoswald.com
- http://www.meetyourmx.com /* Scroll Functioning */
- http://www.pica-pic.com
- http://makeyourmoneymatter.org/

## UIs and resources: ##
- http://www.1stwebdesigner.com/freebies/free-flat-ui-kits/
- http://demo.qodeinteractive.com/simplicity
- http://designmodo.github.io/Flat-UII

## Behance: ##
- http://www.behance.net/gallery/PathGather/11353929
- http://www.behance.net/gallery/Mindworks-New-Website/10693687
- http://www.behance.net/gallery/ISORUN-MINIMAL-REDESIGN-ONEPAGE-SITE/11445265
- http://www.behance.net/gallery/Personal-Website-Portfolio-First-Approach/8262213
- http://www.behance.net/gallery/Million-Killer-Ideas/11198093
- http://www.behance.net/gallery/Questure/11136135
- http://www.behance.net/gallery/Calendar-app/11632997
- http://www.behance.net/gallery/Sony-Connected-World/9464139
- http://www.behance.net/gallery/Brink/8144471 /* Responsive */
- http://www.behance.net/gallery/Concept-Grooveshark-Basic-Player/10990193 /* Background */
- http://www.behance.net/gallery/ios7-style-ui-kit/11459737 /* Background */
- http://www.behance.net/gallery/Ishtar-Icons/11664747 /* Random */

## Loading and Logos: ##
- http://cdpn.io/CxliK
- http://codepen.io/lukerichardville/full/kIwFG
- http://cdpn.io/Hwixy
- http://cdpn.io/HLxbf
- http://behance.vo.llnwd.net/profiles6/731307/projects/10693687/841a210d4364e65fbe611274c7f0a3e6.gif

## Menus: ##
- http://tympanus.net/Tutorials/ResponsiveRetinaReadyMenu/
- http://tympanus.net/Development/SidebarTransitions/
- http://tympanus.net/Tutorials/AnimatedContentMenu/

## Page: ##
- http://tympanus.net/Development/PageTransitions/
- http://tympanus.net/Development/SectionSeparators/
- http://www.thepetedesign.com/demos/onepage_scroll_demo.html

## Buttons and links: ##
- http://cdpn.io/yjxso
- http://tympanus.net/Development/CreativeLinkEffects/
- http://tympanus.net/Development/CreativeButtons/
- http://tympanus.net/Development/IconHoverEffects/

## Thumbnail preview -For portfolio-: ##
- http://tympanus.net/Tutorials/CaptionHoverEffects/index3.html
- http://tympanus.net/Tutorials/AnimatedBorderMenus/index6.html
- http://tympanus.net/Development/ThumbnailGridAnimations/#
- http://tympanus.net/Development/GridLoadingEffects/index2.html
- http://tympanus.net/Tutorials/ThumbnailGridExpandingPreview/
- http://tympanus.net/Development/Stapel/index2.html OR http://mixitup.io/

## Typography effects: ##
http://tympanus.net/Development/OpeningSequence/
http://tympanus.net/Tutorials/BlurMenu/index5.html#
http://tympanus.net/Tutorials/TypographyEffects/index6.html
http://tympanus.net/Tutorials/NaturalLanguageForm/?
http://tympanus.net/Tutorials/OriginalHoverEffects/index6.html
http://tympanus.net/Tutorials/CSS3RotatingWords/index5.html

## Popup: ##
http://tympanus.net/Development/ModalWindowEffects/

## Misc: ##
http://tympanus.net/Tutorials/CreativeCSS3AnimationMenus/